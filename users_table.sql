CREATE TABLE `test`.`users` (
`id` INT(8) NOT NULL AUTO_INCREMENT PRIMARY KEY,
`name` VARCHAR(25) NOT NULL,
`age` INT NOT NULL,
`admin` BIT(1) NOT NULL DEFAULT false,
`createddate` TIMESTAMP DEFAULT CURRENT_TIMESTAMP  NOT NULL);


INSERT INTO `test`.`users` (`name`, `age`, `admin`) VALUES ('Agg', '22', 1),
('Albina', '17', 1),('A', '17', 0),('A', '37', 0),
('Balet', '43', 0),('Bulkina', '71', 0),('Berva', '27', 0),
('Cedric', '22', 0),('Cemka', '27', 0),('Casha', '37', 1),
('Anna', '27', 0),('Egar', '11', 0),('Eda', '22', 0),
('Dena', '27', 1),('Fedor', '43', 1),('Anna', '11', 1),
('Base', '55', 0),('Lego', '33', 0),('Urs', '55', 1),
('Gonduras', '65', 0),('Linda', '88', 1),('Heros', '33', 1),
('Maska', '27', 0),('Reno', '11', 0),('Weda', '55', 1),
('Fera', '55', 1),('Deva', '74', 0),('Feramon', '44', 1),
('Beno', '27', 0),('Verjus', '65', 1),('Dede', '22', 1),
('Mumu', '21', 1),('Eqa', '43', 0),('Jrssica', '44', 1),
('Hanna', '41', 0),('Marina', '43', 1),('Milka', '25', 1),
('Bella', '61', 1),('Agu', '33', 0),('Kollin', '34', 1);
-- select all : execute selected text as one Statement

INSERT INTO `test`.`users` (`name`, `age`, `admin`) VALUES ('Abba', '23', 1);
INSERT INTO `test`.`users` (`name`, `age`) VALUES ('Fefe', '11');
INSERT INTO `test`.`users` (`name`, `age`, `admin`) VALUES ('Gege', '44', 1);